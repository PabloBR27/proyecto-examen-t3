#pragma checksum "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\Ejercicios\muestraRutinas.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "619afe276b22a6088fa9087f812e19864a36a360"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Ejercicios_muestraRutinas), @"mvc.1.0.view", @"/Views/Ejercicios/muestraRutinas.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\_ViewImports.cshtml"
using ProyectoT3;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\_ViewImports.cshtml"
using ProyectoT3.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"619afe276b22a6088fa9087f812e19864a36a360", @"/Views/Ejercicios/muestraRutinas.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"056eb34fff02db339712be61c6e31756964b62cd", @"/Views/_ViewImports.cshtml")]
    public class Views_Ejercicios_muestraRutinas : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<html>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "619afe276b22a6088fa9087f812e19864a36a3603317", async() => {
                WriteLiteral("\r\n    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x\" crossorigin=\"anonymous\">\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "619afe276b22a6088fa9087f812e19864a36a3604509", async() => {
                WriteLiteral("\r\n    <div>\r\n        <a class=\"btn btn-dark\" href=\"/Ejercicios/registraRutinaForm\">Crear Rutinas</a>\r\n    </div>\r\n    <br />\r\n\r\n    <div id=\"ListaRutinas\">\r\n");
#nullable restore
#line 12 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\Ejercicios\muestraRutinas.cshtml"
         foreach (var rutinas in Model.listaRutinas)
        {

#line default
#line hidden
#nullable disable
                WriteLiteral("            <div class=\"list-group\">\r\n                <a class=\"list-group-item\"");
                BeginWriteAttribute("href", " href=\"", 549, "\"", 604, 2);
                WriteAttributeValue("", 556, "/Ejercicios/ListaEjerciciosRutina?id=", 556, 37, true);
#nullable restore
#line 15 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\Ejercicios\muestraRutinas.cshtml"
WriteAttributeValue("", 593, rutinas.Id, 593, 11, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">");
#nullable restore
#line 15 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\Ejercicios\muestraRutinas.cshtml"
                                                                                              Write(rutinas.Nombre);

#line default
#line hidden
#nullable disable
                WriteLiteral("</a><br />\r\n            </div>\r\n");
#nullable restore
#line 17 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\Ejercicios\muestraRutinas.cshtml"
        }

#line default
#line hidden
#nullable disable
                WriteLiteral("        </div>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>\r\n<script>\r\n    var rutin;\r\n    var listaE;\r\n    $(document).ready(function () {\r\n\r\n        $(\"#rutina\").on(\'click\', function () {\r\n            rutin = $(this).val();\r\n        })\r\n        listaE = ");
#nullable restore
#line 29 "D:\UPN\Ciclo 7\Diseño y arquitectura de software\T3\ProyectoT3\Views\Ejercicios\muestraRutinas.cshtml"
            Write(Model.ListaEjercicios);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n    })\r\n    $(\'#listaE #ejercicio\').each(function (indice, elemento) {\r\n\r\n        console.log(\'El elemento con el índice \' + indice + \' contiene \' + $(elemento).text());\r\n    });\r\n</script>\r\n\r\n    \r\n   \r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
