USE [T3]
GO
/****** Object:  Table [dbo].[Ejercicio]    Script Date: 12/06/2021 11:40:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ejercicio](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NULL,
	[LinkVideo] [nvarchar](500) NULL,
 CONSTRAINT [PK_Ejercicio] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListaEjercicios]    Script Date: 12/06/2021 11:40:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListaEjercicios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdRutina] [int] NULL,
	[IdEjercicio] [int] NULL,
	[Tiempo] [int] NULL,
	[NombreEjercicio] [nvarchar](50) NULL,
	[LinkVideo] [nvarchar](500) NULL,
 CONSTRAINT [PK_ListaEjercicios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rutina]    Script Date: 12/06/2021 11:40:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rutina](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_Rutina] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 12/06/2021 11:40:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioU] [nvarchar](50) NULL,
	[Contrasenia] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Ejercicio] ON 
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (1, N'Flexiones', N'https://www.youtube.com/embed/QVvY6liKWVg')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (2, N'Sentadillas', N'https://www.youtube.com/embed/l7aszLSPCVg')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (3, N'Triceps', N'https://www.youtube.com/embed/PYapgguXgT8')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (4, N'Abdominales bicicletas', N'https://www.youtube.com/embed/dYxamPVcKvk')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (5, N'Remo con mancuernas', N'https://www.youtube.com/embed/IKx_Ugn0eCE')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (6, N'Estiramientos', N'https://www.youtube.com/embed/YQQfhILVR7c')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (7, N'La tabla', N'https://www.youtube.com/embed/zfY5XXa26ug')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (8, N'Curl de bíceps alternando brazo', N'https://www.youtube.com/embed/COzFAWnbdPY')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (9, N'Cardio', N'https://www.youtube.com/embed/c8-PMj8gu14')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (10, N'Elevaciones laterales', N'https://www.youtube.com/embed/hgLpdwMtEEs')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (11, N'Zancadas o Lungre', N'https://www.youtube.com/embed/VeWPohDurqk')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (12, N'Estocadas', N'https://www.youtube.com/embed/X61IReICHkA')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (13, N'Jumping', N'https://www.youtube.com/embed/95j1mH27eXc')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (14, N'Puente de glúteos', N'https://www.youtube.com/watch?v=FOE4eoO4nOk')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (15, N'Limpia parabrisas', N'https://www.youtube.com/watch?v=-w9Lhw_ioA0')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (16, N'Superman', N'https://www.youtube.com/watch?v=eu_yOMHpmqA')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (17, N'Plancha alta', N'https://www.youtube.com/watch?v=nR7J73LCL1w')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (18, N'Equilibrio una pierna', N'https://www.youtube.com/watch?v=_3Z2lc_Om7k')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (19, N'Abdominales de pie', N'https://www.youtube.com/watch?v=JWCG90eOvqA')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (20, N'Gemelos', N'https://www.youtube.com/watch?v=KEJOm7blXoE')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (21, N'Sentadilla con una pierna', N'https://www.youtube.com/watch?v=ILO0S9wPNwI')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (22, N'Sentadilla búlgara', N'https://www.youtube.com/watch?v=rlFldyOt0b0')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (23, N'Escalador de montaña', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[Ejercicio] ([Id], [Nombre], [LinkVideo]) VALUES (24, N'Zancada pliométrica', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
SET IDENTITY_INSERT [dbo].[Ejercicio] OFF
GO
SET IDENTITY_INSERT [dbo].[ListaEjercicios] ON 
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (262, 13, 7, 106, N'La tabla', N'https://www.youtube.com/embed/zfY5XXa26ug')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (263, 13, 8, 110, N'Curl de bíceps alternando brazo', N'https://www.youtube.com/embed/COzFAWnbdPY')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (264, 13, 16, 78, N'Superman', N'https://www.youtube.com/watch?v=eu_yOMHpmqA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (265, 13, 2, 82, N'Sentadillas', N'https://www.youtube.com/embed/l7aszLSPCVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (266, 13, 20, 85, N'Gemelos', N'https://www.youtube.com/watch?v=KEJOm7blXoE')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (267, 14, 13, 106, N'Jumping', N'https://www.youtube.com/embed/95j1mH27eXc')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (268, 14, 19, 73, N'Abdominales de pie', N'https://www.youtube.com/watch?v=JWCG90eOvqA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (269, 14, 23, 116, N'Escalador de montaña', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (270, 14, 14, 90, N'Puente de glúteos', N'https://www.youtube.com/watch?v=FOE4eoO4nOk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (271, 14, 20, 109, N'Gemelos', N'https://www.youtube.com/watch?v=KEJOm7blXoE')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (272, 14, 24, 67, N'Zancada pliométrica', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (273, 14, 22, 106, N'Sentadilla búlgara', N'https://www.youtube.com/watch?v=rlFldyOt0b0')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (274, 14, 1, 61, N'Flexiones', N'https://www.youtube.com/embed/QVvY6liKWVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (275, 14, 9, 105, N'Cardio', N'https://www.youtube.com/embed/c8-PMj8gu14')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (276, 14, 11, 120, N'Zancadas o Lungre', N'https://www.youtube.com/embed/VeWPohDurqk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (277, 15, 3, 120, N'Triceps', N'https://www.youtube.com/embed/PYapgguXgT8')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (278, 15, 5, 120, N'Remo con mancuernas', N'https://www.youtube.com/embed/IKx_Ugn0eCE')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (279, 15, 6, 120, N'Estiramientos', N'https://www.youtube.com/embed/YQQfhILVR7c')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (280, 15, 14, 120, N'Puente de glúteos', N'https://www.youtube.com/watch?v=FOE4eoO4nOk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (281, 15, 11, 120, N'Zancadas o Lungre', N'https://www.youtube.com/embed/VeWPohDurqk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (282, 15, 12, 120, N'Estocadas', N'https://www.youtube.com/embed/X61IReICHkA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (283, 15, 22, 120, N'Sentadilla búlgara', N'https://www.youtube.com/watch?v=rlFldyOt0b0')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (284, 15, 2, 120, N'Sentadillas', N'https://www.youtube.com/embed/l7aszLSPCVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (285, 15, 7, 120, N'La tabla', N'https://www.youtube.com/embed/zfY5XXa26ug')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (286, 15, 15, 120, N'Limpia parabrisas', N'https://www.youtube.com/watch?v=-w9Lhw_ioA0')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (287, 15, 17, 120, N'Plancha alta', N'https://www.youtube.com/watch?v=nR7J73LCL1w')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (288, 15, 23, 120, N'Escalador de montaña', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (289, 15, 13, 120, N'Jumping', N'https://www.youtube.com/embed/95j1mH27eXc')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (290, 15, 24, 120, N'Zancada pliométrica', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (291, 15, 1, 120, N'Flexiones', N'https://www.youtube.com/embed/QVvY6liKWVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (292, 16, 13, 100, N'Jumping', N'https://www.youtube.com/embed/95j1mH27eXc')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (293, 16, 9, 83, N'Cardio', N'https://www.youtube.com/embed/c8-PMj8gu14')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (294, 16, 15, 95, N'Limpia parabrisas', N'https://www.youtube.com/watch?v=-w9Lhw_ioA0')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (295, 16, 6, 111, N'Estiramientos', N'https://www.youtube.com/embed/YQQfhILVR7c')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (296, 16, 7, 87, N'La tabla', N'https://www.youtube.com/embed/zfY5XXa26ug')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (297, 17, 23, 107, N'Escalador de montaña', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (298, 17, 21, 81, N'Sentadilla con una pierna', N'https://www.youtube.com/watch?v=ILO0S9wPNwI')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (299, 17, 22, 99, N'Sentadilla búlgara', N'https://www.youtube.com/watch?v=rlFldyOt0b0')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (300, 17, 9, 110, N'Cardio', N'https://www.youtube.com/embed/c8-PMj8gu14')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (301, 17, 2, 77, N'Sentadillas', N'https://www.youtube.com/embed/l7aszLSPCVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (302, 17, 6, 93, N'Estiramientos', N'https://www.youtube.com/embed/YQQfhILVR7c')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (303, 17, 4, 104, N'Abdominales bicicletas', N'https://www.youtube.com/embed/dYxamPVcKvk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (304, 17, 10, 90, N'Elevaciones laterales', N'https://www.youtube.com/embed/hgLpdwMtEEs')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (305, 17, 13, 116, N'Jumping', N'https://www.youtube.com/embed/95j1mH27eXc')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (306, 17, 12, 92, N'Estocadas', N'https://www.youtube.com/embed/X61IReICHkA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (307, 18, 10, 120, N'Elevaciones laterales', N'https://www.youtube.com/embed/hgLpdwMtEEs')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (308, 18, 20, 120, N'Gemelos', N'https://www.youtube.com/watch?v=KEJOm7blXoE')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (309, 18, 14, 120, N'Puente de glúteos', N'https://www.youtube.com/watch?v=FOE4eoO4nOk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (310, 18, 4, 120, N'Abdominales bicicletas', N'https://www.youtube.com/embed/dYxamPVcKvk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (311, 18, 22, 120, N'Sentadilla búlgara', N'https://www.youtube.com/watch?v=rlFldyOt0b0')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (312, 18, 11, 120, N'Zancadas o Lungre', N'https://www.youtube.com/embed/VeWPohDurqk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (313, 18, 19, 120, N'Abdominales de pie', N'https://www.youtube.com/watch?v=JWCG90eOvqA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (314, 18, 3, 120, N'Triceps', N'https://www.youtube.com/embed/PYapgguXgT8')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (315, 18, 2, 120, N'Sentadillas', N'https://www.youtube.com/embed/l7aszLSPCVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (316, 18, 8, 120, N'Curl de bíceps alternando brazo', N'https://www.youtube.com/embed/COzFAWnbdPY')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (317, 18, 5, 120, N'Remo con mancuernas', N'https://www.youtube.com/embed/IKx_Ugn0eCE')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (318, 18, 1, 120, N'Flexiones', N'https://www.youtube.com/embed/QVvY6liKWVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (319, 18, 9, 120, N'Cardio', N'https://www.youtube.com/embed/c8-PMj8gu14')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (320, 18, 12, 120, N'Estocadas', N'https://www.youtube.com/embed/X61IReICHkA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (321, 18, 18, 120, N'Equilibrio una pierna', N'https://www.youtube.com/watch?v=_3Z2lc_Om7k')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (322, 19, 16, 120, N'Superman', N'https://www.youtube.com/watch?v=eu_yOMHpmqA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (323, 19, 4, 120, N'Abdominales bicicletas', N'https://www.youtube.com/embed/dYxamPVcKvk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (324, 19, 17, 120, N'Plancha alta', N'https://www.youtube.com/watch?v=nR7J73LCL1w')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (325, 19, 19, 120, N'Abdominales de pie', N'https://www.youtube.com/watch?v=JWCG90eOvqA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (326, 19, 7, 120, N'La tabla', N'https://www.youtube.com/embed/zfY5XXa26ug')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (327, 19, 24, 120, N'Zancada pliométrica', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (328, 19, 21, 120, N'Sentadilla con una pierna', N'https://www.youtube.com/watch?v=ILO0S9wPNwI')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (329, 19, 11, 120, N'Zancadas o Lungre', N'https://www.youtube.com/embed/VeWPohDurqk')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (330, 19, 23, 120, N'Escalador de montaña', N'https://www.youtube.com/watch?v=qaIivuO66b4')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (331, 19, 12, 120, N'Estocadas', N'https://www.youtube.com/embed/X61IReICHkA')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (332, 19, 18, 120, N'Equilibrio una pierna', N'https://www.youtube.com/watch?v=_3Z2lc_Om7k')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (333, 19, 13, 120, N'Jumping', N'https://www.youtube.com/embed/95j1mH27eXc')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (334, 19, 2, 120, N'Sentadillas', N'https://www.youtube.com/embed/l7aszLSPCVg')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (335, 19, 9, 120, N'Cardio', N'https://www.youtube.com/embed/c8-PMj8gu14')
GO
INSERT [dbo].[ListaEjercicios] ([Id], [IdRutina], [IdEjercicio], [Tiempo], [NombreEjercicio], [LinkVideo]) VALUES (336, 19, 10, 120, N'Elevaciones laterales', N'https://www.youtube.com/embed/hgLpdwMtEEs')
GO
SET IDENTITY_INSERT [dbo].[ListaEjercicios] OFF
GO
SET IDENTITY_INSERT [dbo].[Rutina] ON 
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (13, 2, N'Nueva Rutina')
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (14, 2, N'Nueva Rutina Intermedia')
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (15, 2, N'Nueva Rutina Avanzada')
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (16, 3, N'Rutina del Lunes')
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (17, 3, N'Rutina de los domingos')
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (18, 3, N'Rutina de los martes')
GO
INSERT [dbo].[Rutina] ([Id], [IdUser], [Nombre]) VALUES (19, 3, N'Nueva Rutina de la semana')
GO
SET IDENTITY_INSERT [dbo].[Rutina] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 
GO
INSERT [dbo].[Usuario] ([Id], [UsuarioU], [Contrasenia]) VALUES (1, N'Pablo', N'NieQminDE4Ggcewn98nKl3Jhgq7Smn3dLlQ1MyLPswq7njpt8qwsIP4jQ2MR1nhWTQyNMFkwV19g4tPQSBhNeQ==')
GO
INSERT [dbo].[Usuario] ([Id], [UsuarioU], [Contrasenia]) VALUES (2, N'Rodrigo', N'NieQminDE4Ggcewn98nKl3Jhgq7Smn3dLlQ1MyLPswq7njpt8qwsIP4jQ2MR1nhWTQyNMFkwV19g4tPQSBhNeQ==')
GO
INSERT [dbo].[Usuario] ([Id], [UsuarioU], [Contrasenia]) VALUES (3, N'Rodrigo1', N'NieQminDE4Ggcewn98nKl3Jhgq7Smn3dLlQ1MyLPswq7njpt8qwsIP4jQ2MR1nhWTQyNMFkwV19g4tPQSBhNeQ==')
GO
SET IDENTITY_INSERT [dbo].[Usuario] OFF
GO
