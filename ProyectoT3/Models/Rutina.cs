﻿using ProyectoT3.Models;
using System.Collections.Generic;

namespace ProyectoT3.Models
{
    public class Rutina
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public string Nombre { get; set; }
    }
}
