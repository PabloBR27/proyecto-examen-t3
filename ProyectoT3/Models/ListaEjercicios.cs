﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoT3.Models
{
    public class ListaEjercicios
    {
        public int Id { get; set; }
        public int IdRutina { get; set; }
        public int IdEjercicio { get; set; }
        public int Tiempo { get; set; }
        public string NombreEjercicio { get; set; }
        public string LinkVideo { get; set; }
    }
}
