﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoT3.Models
{
    public class RutinaEjercicios
    {
        public int Id { get; set; }
        public ListaEjercicios listaEjercicios { get; set; }
        public List<Ejercicio> lEjercicios { get; set; }
    }
}
