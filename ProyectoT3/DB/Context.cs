﻿using ProyectoT3.Models;
using ProyectoT3.DB.Mapping;
using Microsoft.EntityFrameworkCore;

namespace ProyectoT3.DB
{
    public class Context : DbContext
    {
        public DbSet<Ejercicio> ejercicios { get; set; }
        public DbSet<Rutina> rutina { get; set; }
        public DbSet<ListaEjercicios> listaEjerciciosCtxt { get; set; }
        public DbSet<Usuario> usuarios { get; set; }

        public Context(DbContextOptions<Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new EjercicioMap());
            modelBuilder.ApplyConfiguration(new ListaEjercicioMap());
            modelBuilder.ApplyConfiguration(new RutinaMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
        }
    }
}
