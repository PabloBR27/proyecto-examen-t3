﻿using Microsoft.EntityFrameworkCore;
using ProyectoT3.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace ProyectoT3.DB.Mapping
{
    public class EjercicioMap : IEntityTypeConfiguration<Ejercicio>
    {
        public void Configure(EntityTypeBuilder<Ejercicio> builder)
        {
            builder.ToTable("Ejercicio");
            builder.HasKey(a => a.Id);
        }
    }
}
