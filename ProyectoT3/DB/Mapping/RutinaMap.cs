﻿using Microsoft.EntityFrameworkCore;
using ProyectoT3.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyectoT3.DB.Mapping
{
    public class RutinaMap : IEntityTypeConfiguration<Rutina>
    {
        public void Configure(EntityTypeBuilder<Rutina> builder)
        {
            builder.ToTable("Rutina");
            builder.HasKey(a => a.Id);
        }
    }
}
