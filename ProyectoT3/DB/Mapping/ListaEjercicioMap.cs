﻿using Microsoft.EntityFrameworkCore;
using ProyectoT3.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyectoT3.DB.Mapping
{
    public class ListaEjercicioMap : IEntityTypeConfiguration<ListaEjercicios>
    {
        public void Configure(EntityTypeBuilder<ListaEjercicios> builder)
        {
            builder.ToTable("ListaEjercicios");
            builder.HasKey(a => a.Id);
        }
    }
}
