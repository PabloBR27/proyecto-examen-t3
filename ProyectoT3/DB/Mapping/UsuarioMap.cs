﻿using Microsoft.EntityFrameworkCore;
using ProyectoT3.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyectoT3.DB.Mapping
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario");
            builder.HasKey(a => a.Id);
        }
    }
}
