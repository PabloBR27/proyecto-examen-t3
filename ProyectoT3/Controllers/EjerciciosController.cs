﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProyectoT3.Models;
using ProyectoT3.DB;
using ProyectoT3.Controllers.Strategy;
using Microsoft.AspNetCore.Authorization;

namespace ProyectoT3.Controllers
{
    [Authorize]
    public class infEjercicio//para mostrar los ejercicios de la rutina con sus tiempos en la vista
    {
        public string NombreEjercicio { get; set; }
        public int TiempoEjercicio { get; set; }
        public string LinkVideo { get; set; }
    }
    public class InfoRutina{
        public List<ListaEjercicios> listaDeEjercicios { get; set; }
        public List<Ejercicio> losEjercicios { get; set; }
    }
    public class InfRutinaTotal
    {
        public List<ListaEjercicios> ejerciciosRutina { get; set; }
        public List<Ejercicio> ejercicios { get; set; }
        public int duracionRutina { get; set; }
        public string Nive { get; set; }
    }
    public class informacionRutina//<<<<<<<<<<<<<<<<<<<<<<<<<
    {
        public List<Rutina> listaRutinas { get; set; }
        public List<ListaEjercicios> ListaEjercicios { get; set; }
        public string Nombre { get; set; }
    }
    [Authorize]
    public class EjerciciosController : Controller
    {
        public static InfRutinaTotal laRutina = new InfRutinaTotal();
        private Context _contexto;
        public EjerciciosController(Context _context)
        {
            this._contexto = _context;
        }

        public List<infEjercicio> ElegirRutina( IEstrategia estrategia)
        {
            ContextoS _context = new ContextoS(this._contexto);
            _context.setEstrategia(estrategia);
            

            List<ListaEjercicios> ejercicios = _context.creaRutina();
            List<Ejercicio> listEjerccios = new List<Ejercicio>();
            List<infEjercicio> listInfEjercicio = new List<infEjercicio>();
            Ejercicio ejercicio;
            infEjercicio infoEjercicio;
            foreach (ListaEjercicios lEjer in ejercicios)
            {
                ejercicio = _contexto.ejercicios.First(e => e.Id == lEjer.IdEjercicio);
                infoEjercicio = new infEjercicio()
                {
                    NombreEjercicio = ejercicio.Nombre,
                    TiempoEjercicio = lEjer.Tiempo,
                    LinkVideo = ejercicio.LinkVideo
                };
                //listEjerccios.Add(_contexto.ejercicios.First(e=>e.Id == lEjer.IdEjercicio));
                listEjerccios.Add(ejercicio);
                listInfEjercicio.Add(infoEjercicio);

            }
            //Tiempo total de la lista
            int tiempoTotal = 0;
            foreach(ListaEjercicios e in ejercicios)
            {
                tiempoTotal += e.Tiempo;
            }

            InfoRutina ejerciciosRutina = new InfoRutina()
            {
                listaDeEjercicios = ejercicios,
                losEjercicios = listEjerccios
            };

            setRutina(tiempoTotal, listEjerccios, ejercicios);

            return listInfEjercicio;
        }
        [HttpGet]
        public IActionResult nuevaRutina()
        {
            return View("ElegirRutina");
        }
        //Rutina páginas después de login
        public IActionResult muestraRutinas()
        {



            var user = GetLoggedUser();
            var Rutinas = _contexto.rutina.Where(r => r.IdUser == GetLoggedUser().Id);
            var Ejercicios = _contexto.ejercicios;
            var ListEjercicios = _contexto.listaEjerciciosCtxt;
            List<Rutina> listaRutinas = new List<Rutina>();
            List<ListaEjercicios> listaEjercicios = new List<ListaEjercicios>();
            //Rutinas del usaurio
            foreach (Rutina rutina in Rutinas)
            {
                Rutina newRutina = Rutinas.First(r=>r.Id == rutina.Id);
                listaRutinas.Add(newRutina);
            }

            List<Ejercicio> listEjerciciosRutina = new List<Ejercicio>();
            //ejercicios de la rutina
            foreach (Rutina rutina in listaRutinas)
            {
                listaEjercicios = ListEjercicios.Where(e => e.IdRutina == rutina.Id).ToList();
            }
            informacionRutina rutinas = new informacionRutina()
            {
                listaRutinas = listaRutinas,
                ListaEjercicios = listaEjercicios
            };

            return View("muestraRutinas", rutinas);
        }
        [HttpGet]
        public IActionResult registraRutinaForm()
        {
            return View();
        }
        //
        [HttpPost]
        public IActionResult registrarRutina(string nombre, string Nivel)
        {
            ContextoS _context = new ContextoS(this._contexto);
            
            //guardamos la rutina para luego obtener su Id
            Rutina rut = new Rutina()
            {
                Nombre = nombre,
                IdUser = GetLoggedUser().Id
            };
            _contexto.rutina.Add(rut);
            _contexto.SaveChanges();

            //escogemos la estrategia para los ejercicios
            switch (Nivel)
            {
                case "Principiante": _context.setEstrategia(new Principiante(this._contexto)); break;
                case "Intermedio": _context.setEstrategia(new Intermedio(this._contexto)); break;
                case "Avanzado": _context.setEstrategia(new Avanzado(this._contexto)); break;
            }
            //Obtenemos los ejercicios
            List<infEjercicio> listInfEjercicio = ElegirRutina(_context.getEstrategia());
            registraRutina();
            return View("ElegirRutina", listInfEjercicio);
        }
        //registra la rutina
        [HttpPost]
        public void registraRutina()
        {
            
            var rutinaTemp = _contexto.rutina;
            List<Rutina> lTemRutina = rutinaTemp.ToList();
            Rutina ruti = lTemRutina.LastOrDefault();

            foreach(ListaEjercicios ejercicio in laRutina.ejerciciosRutina)
            {
                ejercicio.IdRutina = ruti.Id;
                _contexto.listaEjerciciosCtxt.Add(ejercicio);
            }
            _contexto.SaveChanges();

            //return RedirectToAction("ElegirRutina",_contexto);
        }
        public IActionResult ListaEjerciciosRutina(int id)
        {
            var user = GetLoggedUser();
            //var Rutinas = _contexto.rutina.FirstOrDefault(r => r.IdUser == GetLoggedUser().Id);
            var ListEjercicios = _contexto.listaEjerciciosCtxt;

            List<ListaEjercicios> lista = ListEjercicios.ToList();
            List<Rutina> listaRutinas = new List<Rutina>();
            List<ListaEjercicios> listaEjercicios = new List<ListaEjercicios>();
            
            //Rutinas del usaurio
            List<Ejercicio> listEjerciciosRutina = new List<Ejercicio>();
            listaEjercicios = ListEjercicios.Where(l=>l.IdRutina == id).ToList();
            return View("ListaEjerciciosRutina", listaEjercicios);
        }

        //Otras funciones
        public void setRutina(int duracion, List<Ejercicio> ejercicios, List<ListaEjercicios> listaEjercicios)
        {
            laRutina.ejercicios = ejercicios;
            laRutina.ejerciciosRutina = listaEjercicios;
        }
        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _contexto.usuarios.First(o => o.UsuarioU == username);
            return user;
        }
    }
}
