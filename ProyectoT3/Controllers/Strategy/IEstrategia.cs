﻿using ProyectoT3.Models;
using System.Collections.Generic;
using ProyectoT3.DB;

namespace ProyectoT3.Controllers.Strategy
{
    public abstract class IEstrategia
    {
        public abstract List<ListaEjercicios> CreaRutina();
        public abstract void RegistraRutina();
    }
}
