﻿using Microsoft.AspNetCore.Mvc;
using ProyectoT3.DB;
using ProyectoT3.Models;
using ProyectoT3.Controllers.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//Se seleccionará 5 ejercicios al azar
//y cada ejericio se debe poner un
//tiempo de entre 60 a 120 segundos por ejercicio 
namespace ProyectoT3.Controllers.Strategy
{
    public class Principiante: IEstrategia
    {
        private Context _context;
        public Principiante(Context _context)
        {
            this._context = _context;
        }
        public override List<ListaEjercicios> CreaRutina()
        {
            var ejercicios = _context.ejercicios;

            int tamanio = ejercicios.Count();
            int idAleatorio;
            List<int> IdsAleatorios = new List<int>();
            while (IdsAleatorios.Count < 5 )//lista de ids aleatorios
            {
                Random numRan = new Random();
                idAleatorio = numRan.Next(1, tamanio);
                while (IdsAleatorios.Contains(idAleatorio))//nuevo si se repita
                {
                    numRan = new Random();
                    idAleatorio = numRan.Next(1, tamanio +1);
                }
                IdsAleatorios.Add(idAleatorio);
            }
            List<Ejercicio> listaEjercicio = new List<Ejercicio>();
            List<ListaEjercicios> listEjercicios = new List<ListaEjercicios>();
            
            foreach (int i in IdsAleatorios)//asiganmos ejerccios por ID
            {
                foreach (Ejercicio ejr in ejercicios)
                {
                    if (ejr.Id == i)//ejerccio a lista
                    {
                        /*Tiempo de 60 a 120 segundos*/
                        Random randomTiempo = new Random();
                        int tiempo = randomTiempo.Next(60, 121);
                        listaEjercicio.Add(ejr);
                        ListaEjercicios newEjerccios = new ListaEjercicios
                        {
                            IdRutina = 1,
                            IdEjercicio = ejr.Id,
                            Tiempo = tiempo,
                            NombreEjercicio = ejr.Nombre,
                            LinkVideo = ejr.LinkVideo
                        };
                        listEjercicios.Add(newEjerccios);
                    }
                }
            }
            return listEjercicios;
        }
        public override void RegistraRutina()
        {

        }
    }
}
