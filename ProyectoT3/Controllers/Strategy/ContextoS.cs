﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProyectoT3.Controllers.Strategy;
using ProyectoT3.Models;
using ProyectoT3.DB;

namespace ProyectoT3.Controllers.Strategy
{
    public class ContextoS
    {
        private IEstrategia _estrategia;
        private Context _context;
        public ContextoS(Context _context)
        {
            this._context = _context;
        }
        public ContextoS()
        {
            this._estrategia = new Intermedio(this._context);
        }

        public void setEstrategia(IEstrategia estrategia)
        {
            this._estrategia = estrategia;
        }
        public IEstrategia getEstrategia()
        {
            return this._estrategia;
        }

        public List<ListaEjercicios> creaRutina()
        {
            return this._estrategia.CreaRutina();
        }
        public void registraRutina()
        {
            this._estrategia.RegistraRutina();
        }

    }
}
