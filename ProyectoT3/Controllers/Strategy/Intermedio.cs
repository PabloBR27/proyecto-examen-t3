﻿using Microsoft.AspNetCore.Mvc;
using ProyectoT3.DB;
using ProyectoT3.DB.Mapping;
using ProyectoT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoT3.Controllers.Strategy
{
    public class Intermedio : IEstrategia
    {
        private Context _context;
        public Intermedio(Context _context)
        {
            this._context = _context;
        }
        public override List<ListaEjercicios> CreaRutina() {
            var ejercicios = _context.ejercicios;
            
            int tamanio = ejercicios.Count();
            int idAleatorio;
            
            List<int> IdsAleatorios = new List<int>();
            while (IdsAleatorios.Count < 10 )//lista de ejercicios aleatorios
            {
                Random numRan = new Random();
                idAleatorio = numRan.Next(1,tamanio +1);
                while (IdsAleatorios.Contains(idAleatorio))//nuevo si se repita
                {
                    numRan = new Random();
                    idAleatorio = numRan.Next(1, tamanio);
                }
                IdsAleatorios.Add(idAleatorio);
            }
            List<Ejercicio> listaEjercicio = new List<Ejercicio>();
            List<ListaEjercicios> listEjercicios = new List<ListaEjercicios>();

            foreach(int i in IdsAleatorios)//asiganmos ejerccios por ID
            {
                foreach(Ejercicio ejr in ejercicios)
                {
                    if (ejr.Id == i)//ejerccio a lista
                    {
                        /*Tiempo de 60 a 120 segundos*/
                        Random randomTiempo = new Random();
                        int tiempo = randomTiempo.Next(60, 121);
                        listaEjercicio.Add(ejr);
                        ListaEjercicios newEjerccios = new ListaEjercicios
                        {
                            IdRutina = 1,
                            IdEjercicio = ejr.Id,
                            Tiempo = tiempo,
                            NombreEjercicio = ejr.Nombre,
                            LinkVideo = ejr.LinkVideo
                        };
                        listEjercicios.Add(newEjerccios);
                    }
                }
            }

            return listEjercicios;
        }
        public override void RegistraRutina()
        {

        }

    }
}
