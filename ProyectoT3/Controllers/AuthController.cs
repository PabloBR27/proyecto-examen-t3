﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Security.Claims;
using ProyectoT3.DB;
using ProyectoT3.Models;
using System.Text;

namespace Proyecto_Final.Controllers
{
    public class AuthController : Controller
    {
        private Context context;

        private IConfiguration configuration;

        public AuthController(Context context, IConfiguration config)
        {
            this.context = context;
            this.configuration = config;
        }
        /**/
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password, string tp)
        {
            Usuario user = context.usuarios
                .FirstOrDefault(o => o.UsuarioU == username && o.Contrasenia == CreateHash(password));
            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o contraseña incorrecto";
                return RedirectToAction("Login");
            }
            //Autenticación del usuario
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UsuarioU),
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                HttpContext.SignInAsync(claimsPrincipal);
            return RedirectToAction("muestraRutinas", "Ejercicios");
        }
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
        /**/
        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
        /**/
        [HttpGet]
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearCuenta(string username, string password)
        {
            string pass = CreateHash(password);
            // no hay mensajes => 0 mensaje
            Usuario user = new Usuario
            {
                UsuarioU = username,
                Contrasenia = pass,
            };

            context.usuarios.Add(user);
            context.SaveChanges();
            return View();
        }

    public IActionResult Index()
        {
            return View();
        }
    }
}
